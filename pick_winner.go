package main

import (
	"fmt"
	"math"
	"time"
)

//data field consists of howLongAgo, emaVsSma, growthDiff, positiveGrowthCheck
type coinDataStruct struct {
	Name string
	Data []float64
}

//these are proper data types as given by POLO's API LOL
type ticker struct {
	ID            int    `json:"id"`
	Last          string `json:"last"`
	LowestAsk     string `json:"lowestAsk"`
	HighestBid    string `json:"highestBid"`
	PercentChange string `json:"percentChange"`
	BaseVolume    string `json:"baseVolume"`
	QuoteVolume   string `json:"quoteVolume"`
	IsFrozen      string `json:"isFrozen"`
	High24Hr      string `json:"high24hr"`
	Low24Hr       string `json:"low24hr"`
}

//ChartStruct - json of an element in the chart data api
type ChartStruct struct {
	Date            float64 `json:"date"`
	High            float64 `json:"high"`
	Low             float64 `json:"low"`
	Open            float64 `json:"open"`
	Close           float64 `json:"close"`
	Volume          float64 `json:"volume"`
	QuoteVolume     float64 `json:"quoteVolume"`
	WeightedAverage float64 `json:"weightedAverage"`
}

//OrderBookStruct - json for orderbook api call for single coinpair
type OrderBookStruct struct {
	Asks     [][]interface{} `json:"asks"`
	Bids     [][]interface{} `json:"bids"`
	IsFrozen string          `json:"isFrozen"`
	Seq      int             `json:"seq"`
}

//ExponentialMovingAverage - gets the EMA for given array of numbers
func ExponentialMovingAverage(arrayOfValues []float64, window int, smaFirstElement float64) ([]float64, error) {
	ema := make([]float64, len(arrayOfValues)-window+1)
	multiplier := 2 / (float64(window) + 1)
	ema[0] = smaFirstElement
	for i := 1; i < len(ema); i++ {
		//values of today are calculated from values of yesterday
		ema[i] = ((arrayOfValues[i+window-1] - ema[i-1]) * multiplier) + ema[i-1]
	}
	return ema, nil
}

//HowLongAgoCross - exported for testing
func HowLongAgoCross(ema, sma []float64) (index, howLongAgo int) {
	//mismatched lenghts of arrays may lead to problems
	if len(ema) != len(sma) {
		fmt.Println("please make sure that lengths of ema and sma are equal")
		return 0, -1
	}
	for i := len(sma) - 1; i >= 1; i-- {
		if (sma[i] < ema[i]) && (sma[i-1] > ema[i-1]) {
			howLongAgo := len(sma) - i
			indexOfCrossing := i
			return indexOfCrossing, howLongAgo
		}
	}
	return -1, -1

}

// getAverageGrowthRate - given an array and the last n number of values to examine
//returns the average distance of n-1 distances
func getAverageGrowthRate(arrayOfValues []float64, lastValues int) float64 {
	sumOfDistances := float64(0)
	var distance float64
	for i := len(arrayOfValues) - lastValues; i < len(arrayOfValues)-1; i++ {
		distance = arrayOfValues[i+1] - arrayOfValues[i]
		sumOfDistances += distance
	}
	return (sumOfDistances / (float64(lastValues) - 1))
}

//isEmaOverSma - returns 1 is ema[-1] > sma[-1] ; else 0
func checkIfEmaGreaterThanSma(ema []float64, sma []float64) int {
	if ema[len(ema)-1] > sma[len(sma)-1] {
		return 1
	}
	return 0
}

//isGrowthLessThanTol - returns 1 if ema isnt growing much quicker than sma
//returns 0 if ema is growing too quick compaired to sma
func isGrowthLessThanTol(ema []float64, sma []float64, lastValues int, tolerance float64) int {
	emaGrowth := getAverageGrowthRate(ema, lastValues)
	smaGrowth := getAverageGrowthRate(sma, lastValues)
	if emaGrowth < smaGrowth*(1+tolerance) {
		return 1
	}
	return 0
}

//getGrowthCompairison - returns how much % ema growth is bigger than sma's
func getGrowthCompairison(ema []float64, sma []float64, lastValues int) float64 {
	emaGrowth := getAverageGrowthRate(ema, lastValues)
	smaGrowth := getAverageGrowthRate(sma, lastValues)
	percentOver := emaGrowth * 100 / smaGrowth
	if percentOver > 0 {
		return percentOver - 100
	}
	return 0
}

func isGrowthPositive(array []float64, sinceWhen int) int {
	indexOfSlice := len(array) - sinceWhen
	if sinceWhen == -1 {
		return 0
	}
	workingArray := array[indexOfSlice:]
	for i := 0; i < len(workingArray)-1; i++ {
		if workingArray[i] > workingArray[i+1] {
			return 0
		}
	}
	return 1
}

//getCoinDataForComparison - calculates scores for comparison metrics between coins
func getCoinDataForComparison(coinName, startDate, period string) (coinDataStruct, error) {
	var coinData coinDataStruct
	//gets data from api call
	chartJSON, err := GetChartData(coinName, startDate, period)
	checkError(err)
	//converts data into json
	chartStruct, err := UnmarshalChartData(chartJSON)
	checkError(err)
	//creates array of all json elements
	closingArray := GetArrayFromField(chartStruct, "Close")
	if len(closingArray) < 30 {
		return coinData, err
	}
	sma, err := SimpleMovingAverage(closingArray, SmaWindow)
	checkError(err)
	ema, err := ExponentialMovingAverage(closingArray, EmaWindow, sma[0])
	checkError(err)

	//only examines the last 100 elements of the arrays
	//also making the arrays the same length may prevent problems
	ema, err = GetLastNElements(ema, 100)
	checkError(err)
	sma, err = GetLastNElements(sma, 100)
	checkError(err)

	_, howLongAgo := HowLongAgoCross(ema, sma)
	emaVsSma := checkIfEmaGreaterThanSma(ema, sma)
	growthDiff := getGrowthCompairison(ema, sma, howLongAgo)
	positiveGrowthCheck := isGrowthPositive(closingArray, howLongAgo)

	coinData.Name = coinName
	coinData.Data = []float64{float64(howLongAgo), float64(emaVsSma), growthDiff, float64(positiveGrowthCheck)}
	return coinData, nil
}

//PickWinner - given startdate and period returns list of all coins traded
//against btc and their given score
func PickWinner(startDate, period string, verbosity bool) string {
	if verbosity == false {
		fmt.Println("analyzing coins")
	}
	//set up for variables
	startDate = DateTimeToEpochTime(startDate)
	coinNames, err := getCoinNames()
	checkError(err)
	winningData := []float64{}
	best := math.MaxFloat64
	var winningCoin string
	//gets comparison metrics for each coin
	//howlongacross is squared
	for _, coinName := range coinNames {
		coin, err := getCoinDataForComparison(coinName, startDate, period)
		time.Sleep(200 * time.Millisecond)
		coinData := coin.Data
		name := coin.Name
		checkError(err)
		// this seems arbitrary. it does belong here, but feels like a tuning parameter and i don't like that
		//adjusting howlongacross to be x^2 to penalize older crosses
		if coinData[0] != -1 {
			coinData[0] = coinData[0] * coinData[0]
		}
		product := ArrayMultiplier(coinData)
		if verbosity == true {
			fmt.Println(name, coinData, product)
		}
		if product > 0 && product < best {
			best = product
			winningCoin = name
			winningData = coinData
		}
	}
	return winningCoin
}
