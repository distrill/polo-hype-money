package main

import (
	"fmt"
	"time"
)

func shouldAcceptBid() bool {
	return true
}

// pump - enter a position by purchasing a coin
func pump(coinPair string) (float64, float64) {
	// god damnit, move this to response_parsing
	bytes, err := GetOrderBook(coinPair)
	checkError(err)
	orderBook, err := UnmarshalOrderBookForSinglePair(bytes)

	ordersToBuy := orderBook.Asks[:3]
	weightedAveragePurchasePrice := 0.0
	amountOfNewCoin := 0.0
	totalBTCSpent := 0.0
	for _, item := range ordersToBuy {
		price, amount := interfaceToFloat(item)
		totalBTCSpent += price * amount
		weightedAveragePurchasePrice += price * amount
		amountOfNewCoin += amount
	}
	weightedAveragePurchasePrice = weightedAveragePurchasePrice / float64(amountOfNewCoin)
	return totalBTCSpent, weightedAveragePurchasePrice
}

// dump - exit position by selling coin
func dump(coinPair string) (float64, float64) {
	amountOfHolding := GetCoinBalance(coinPair)
	fmt.Println("current coin held is : ", coinPair)
	staleCounter := 0
	weightedAverageSellPrice := 0.0
	totalCoinSold := 0.0
	for amountOfHolding > 0 {
		bytes, err := GetOrderBook(coinPair)
		checkError(err)
		orderBook, err := UnmarshalOrderBookForSinglePair(bytes)
		checkError(err)
		price, amount := interfaceToFloat(orderBook.Bids[0])
		if shouldAcceptBid() {
			soldAmount := MakeTrade(coinPair, "sell", amount, price)
			fmt.Println("sold some coin")
			staleCounter = 0
			totalCoinSold += soldAmount
			weightedAverageSellPrice += price * amount
		}
	}
	weightedAverageSellPrice = weightedAverageSellPrice / totalCoinSold
	return totalCoinSold, weightedAverageSellPrice
}

func getSellPoint(price, lossTolerance float64) float64 {
	return price - (price * lossTolerance)
}
