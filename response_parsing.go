package main

import (
	"encoding/json"
	"log"
	"strconv"
	"strings"
)

type chartStruct struct {
	Date            float64 `json:"date"`
	High            float64 `json:"high"`
	Low             float64 `json:"low"`
	Open            float64 `json:"open"`
	Close           float64 `json:"close"`
	Volume          float64 `json:"volume"`
	QuoteVolume     float64 `json:"quoteVolume"`
	WeightedAverage float64 `json:"weightedAverage"`
}

type buyStruct struct {
	OrderNumber     float64 `json:"orderNumber"`
	ResultingTrades []struct {
		Amount  float64 `json:"amount"`
		Date    string  `json:"date"`
		Rate    float64 `json:"rate"`
		Total   float64 `json:"total"`
		TradeID int     `json:"tradeId"`
		Type    string  `json:"type"`
	}
}

func unmarshalBalanceData(balanceBytes []byte) map[string]string {
	balanceData := make(map[string]string)
	decoder := json.NewDecoder(strings.NewReader(string(balanceBytes)))
	if err := decoder.Decode(&balanceData); err != nil {
		log.Fatal(err)
	}
	return balanceData
}

func unmarshalPurchaseData(purchaseBytes []byte) buyStruct {
	purchaseData := buyStruct{}
	decoder := json.NewDecoder(strings.NewReader(string(purchaseBytes)))
	if err := decoder.Decode(&purchaseData); err != nil {
		log.Fatal(err)
	}
	return purchaseData
}

// GetCurrentPrice - get last traded price on polo for a given currency pair
func GetCurrentPrice(currencyPair string) float64 {
	tickerString, err := GetTickers()
	if err != nil {
		log.Fatal(err)
	}

	tickerInfo, err := parseTickerResponse(tickerString)
	if err != nil {
		log.Fatal(err)
	}

	price, err := strconv.ParseFloat(tickerInfo[currencyPair].Last, 64)
	if err != nil {
		log.Fatal(err)
	}

	return price
}

// GetCoinBalance - returns total bitcoin balance in account
func GetCoinBalance(coin string) float64 {
	balanceBytes, err := GetBalances()
	if err != nil {
		log.Fatal(err)
	}

	balanceData := unmarshalBalanceData(balanceBytes)
	balance, err := strconv.ParseFloat(balanceData[coin], 32)
	if err != nil {
		log.Fatal(err)
	}

	return balance
}

// MakeTrade - purchases a given amount of a given coin
// returns the amount of given coin purchased
func MakeTrade(currencyPair, command string, amount, rate float64) float64 {
	purchaseBytes, err := Trade(currencyPair, command, rate, amount)
	if err != nil {
		log.Fatal(err)
	}

	purchaseData := unmarshalPurchaseData(purchaseBytes)
	return purchaseData.ResultingTrades[0].Amount
}

func parseTickerResponse(tickerString []byte) (map[string]ticker, error) {
	tickerInfo := make(map[string]ticker)
	decoder := json.NewDecoder(strings.NewReader(string(tickerString)))
	if err := decoder.Decode(&tickerInfo); err != nil {
		return nil, err
	}
	return tickerInfo, nil
}

func getCoinTickerData(coinPair string) (ticker, error) {
	tickerString, err := GetTickers()
	checkError(err)
	tickerInfo, err := parseTickerResponse(tickerString)
	checkError(err)
	return tickerInfo[coinPair], err
}

//UnmarshalChartData - gets JSON data for chart info for given coin pair
func UnmarshalChartData(chartBytes []byte) ([]ChartStruct, error) {
	var chartCollections []ChartStruct
	err := json.Unmarshal(chartBytes, &chartCollections)
	if err != nil {
		return nil, err
	}
	return chartCollections, nil
}

//UnmarshalOrderBookForSinglePair - gets struct of order book for single coinpair
func UnmarshalOrderBookForSinglePair(byteArray []byte) (OrderBookStruct, error) {
	var orderBook OrderBookStruct
	err := json.Unmarshal(byteArray, &orderBook)
	checkError(err)
	return orderBook, nil
}

//getCoinNames - gets all coins that is traded against BTC
func getCoinNames() ([]string, error) {
	tickerString, err := GetTickers()
	if err != nil {
		return nil, err
	}
	tickerInfo, err := parseTickerResponse(tickerString)
	if err != nil {
		return nil, err
	}
	listOfCoins := make([]string, 0)
	if err != nil {
		return nil, err
	}
	btcString := "BTC_"
	for key := range tickerInfo {
		bitCoinVolume := stringToFloat(tickerInfo[key].BaseVolume)
		if strings.Contains(key, btcString) && key != "BTC_DOGE" {
			if tickerInfo[key].IsFrozen == "0" {
				if bitCoinVolume > 50.0 {
					listOfCoins = append(listOfCoins, key)
				}
			}
		}
	}
	return listOfCoins, nil
}

func getChartData(coinName string) {
	var coinData coinDataStruct
	//gets data from api call
	chartJSON, err := GetChartData(coinName, startDate, period)
	checkError(err)
	//converts data into json
	chartStruct, err := UnmarshalChartData(chartJSON)
}
