package main

// PoloURL - base url for a requests
var PoloURL = "https://poloniex.com/"

//EmaWindow - size of window for calculating exponential moving average
var EmaWindow = 30

//SmaWindow - size of window for calculating simple moving average
var SmaWindow = 50
