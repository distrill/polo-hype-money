package main

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

func private(command string, params url.Values) ([]byte, error) {
	if params == nil {
		params = url.Values{}
	}

	address := PoloURL + "tradingApi"
	nonce := strconv.FormatInt(time.Now().UTC().UnixNano(), 10)

	params.Add("command", command)
	params.Add("nonce", nonce)

	req, err := http.NewRequest("POST", address, strings.NewReader(params.Encode()))
	if err != nil {
		return nil, err
	}

	message := []byte(params.Encode())
	mac := hmac.New(sha512.New, []byte(APISecret))
	mac.Write(message)
	signature := hex.EncodeToString(mac.Sum(nil))

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Key", APIKey)
	req.Header.Add("Sign", signature)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func public(command string, params url.Values) ([]byte, error) {
	address := PoloURL + "/public?command=" + command

	if params != nil {
		address += ("&" + params.Encode())
	}

	res, err := http.Get(address)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

//GetOrderBook - returns list of all offers for coinpair
//maybe merge with public?
func GetOrderBook(coinPair string) ([]byte, error) {
	address := PoloURL + "/public?command=" + "returnOrderBook" + "&currencyPair=" + coinPair

	res, err := http.Get(address)
	checkError(err)

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	checkError(err)

	return body, nil
}

// GetDepositAddresses - get all deposit addresses for given account
func GetDepositAddresses() ([]byte, error) {
	res, err := private("returnDepositAddresses", nil)
	if err != nil {
		return nil, err
	}
	return res, nil
}

// Trade - make a trade of the given currency pair, for the direction, amount and rate specified
func Trade(currencyPair, command string, rate, amount float64) ([]byte, error) {
	if command != "buy" && command != "sell" {
		return nil, errors.New("Invalid command sent to trade request: " + command)
	}
	params := url.Values{}
	params.Add("currencyPair", currencyPair)
	params.Add("rate", strconv.FormatFloat(rate, 'f', -1, 64))
	params.Add("amount", strconv.FormatFloat(amount, 'f', -1, 64))

	res, err := private(command, params)
	if err != nil {
		return nil, err
	}

	return res, nil
}

// GetBalances - get balance information for each coin for given account
func GetBalances() ([]byte, error) {
	res, err := private("returnBalances", nil)
	if err != nil {
		return nil, err
	}
	return res, nil
}

// GetTickers - get all ticker symbols trading on polo
func GetTickers() ([]byte, error) {
	res, err := public("returnTicker", nil)
	if err != nil {
		return nil, err
	}
	return res, nil
}

//GetChartData - get all chart data for given btc_xxx
func GetChartData(coinPair, startDate, period string) ([]byte, error) {
	end := "9999999999"
	commandString := "returnChartData&currencyPair=" + coinPair
	commandString += "&start=" + startDate + "&end=" + end + "&period=" + period
	res, err := public(commandString, nil)
	if err != nil {
		return nil, err
	}
	return res, nil
}
