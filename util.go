package main

import (
	"fmt"
	"log"
	"math"
	"reflect"
	"strconv"
	"time"

	"github.com/go-errors/errors"
)

// this array input is strange. i think i get why, but there gotta be a nicer way
func interfaceToFloat(array []interface{}) (float64, float64) {
	itemZeroInterface := array[0]
	var itemZero float64
	var err error
	if temp, ok := itemZeroInterface.(string); ok {
		itemZero, err = strconv.ParseFloat(temp, 64)
		checkError(err)
	}
	itemOneInterface := array[1]
	var itemOne float64
	if temp, ok := itemOneInterface.(float64); ok {
		itemOne = temp
	}
	if temp, ok := itemOneInterface.(string); ok {
		itemOne = stringToFloat(temp)
	}
	return itemZero, itemOne
}

//CheckError - checks error with less typing
func checkError(err error) {
	if err != nil {
		fmt.Println(err.(*errors.Error).ErrorStack())
		log.Fatal(err)
	}
}

func floatToString(input float64) string {
	return strconv.FormatFloat(input, 'E', 10, 64)
}

func stringToFloat(input string) float64 {
	temp, err := strconv.ParseFloat(input, 64)
	checkError(err)
	return temp
}

func intToString(input int64) string {
	return strconv.FormatInt(input, 10)
}

//DateTimeToEpochTime - given time string with specific layout
//returns seconds since epoch as string
func DateTimeToEpochTime(inputTime string) string {
	layout := "01/02/2006 3:04:05 PM"
	outputTime, err := time.Parse(layout, inputTime)
	if err != nil {
		log.Fatal(err)
	}
	outputTimeString := strconv.FormatInt(outputTime.Unix(), 10)
	return outputTimeString

}

//EpochTimeToDateTime - given epoch time returns string date
func EpochTimeToDateTime(unixTime string) string {
	timetemp, err := strconv.ParseInt(unixTime, 10, 64)
	CheckError(err)
	return time.Unix(timetemp, 0).String()
}

//reflectField - helper function for GetChartField
func reflectField(chartLine *ChartStruct, field string) float64 {
	r := reflect.ValueOf(chartLine)
	f := reflect.Indirect(r).FieldByName(field)
	return f.Interface().(float64)
}

//GetArrayFromField - given array of charts and field name returns array of chartData.fieldname
func GetArrayFromField(chartData []ChartStruct, field string) []float64 {
	valueArray := make([]float64, 0)
	for _, item := range chartData {
		value := reflectField(&item, field)
		valueArray = append(valueArray, value)
	}
	return valueArray
}

//SimpleMovingAverage  - calculates moving average for given array and window
func SimpleMovingAverage(arrayOfValues []float64, windowSize int) ([]float64, error) {
	window := make([]float64, windowSize)
	if len(arrayOfValues) < windowSize {
		zeroes := make([]float64, len(arrayOfValues))
		return zeroes, nil
	}
	for i := 0; i < windowSize; i++ {
		window[i] = arrayOfValues[i]
	}
	numberOfWindows := len(arrayOfValues) - windowSize + 2
	movingAverageArray := make([]float64, numberOfWindows-1)

	for i := 0; i < numberOfWindows-1; i++ {
		movingAverageArray[i] = AverageArray(window)
		//window marching ([3,4,5] -> [4,5,6])
		if i < numberOfWindows-2 { //the 2 isnt a magic number
			for j := 0; j < windowSize-1; j++ {
				window[j] = window[1+j]
			}
			window[windowSize-1] = arrayOfValues[i+windowSize]
		}

	}
	return movingAverageArray, nil
}

//GetLastNElements - returns the last x elements of an array
//returns array of zeroes if size > len(inputArray)
func GetLastNElements(inputArray []float64, size int) ([]float64, error) {
	output := make([]float64, size)
	if len(output) > len(inputArray) {
		return output, nil
	}
	for i := 0; i < size; i++ {
		indexForOutput := len(output) - i - 1
		output[indexForOutput] = inputArray[len(inputArray)-1-i]
	}
	return output, nil
}

//AverageArray - calculates mean for given array
func AverageArray(arrayOfValues []float64) float64 {
	sum := float64(0)
	n := float64(len(arrayOfValues))
	for _, value := range arrayOfValues {
		sum += value
	}
	return (sum / n)
}

//ArrayMultiplier - retunrs the product of each element of the array
func ArrayMultiplier(arrayOfValues []float64) float64 {
	output := float64(1)
	for i := 0; i < len(arrayOfValues); i++ {
		if math.IsNaN(arrayOfValues[i]) {
			return 0.0
		}
		output *= arrayOfValues[i]
	}
	return output
}
