package main

func shouldExitPosition() bool {
	// TODO when is the best time to get out?
	return true
}

func main() {
	// application state
	currentPosition := ""
	positionEntryPrice := float64(0)

	// application logic
	for {
		if currentPosition == "" {
			// if we are not in a position, try to get in one
			currentPosition = PickWinner("start date", "period", true)
				_, positionEntryPrice = pump(currentPosition)
		} else if shouldExitPosition() {
			// if we are in a position, evaluate whether we should exit or not
			btcHolding, positionExitPrice := dump(currentPosition)
			updateWallet(currentPosition, btcHolding, positionEntryPrice, positionExitPrice)
			currentPosition = ""
		}
	}

}
